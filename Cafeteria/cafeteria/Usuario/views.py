from rest_framework import viewsets
from django.contrib.auth.models import User
from .serializers import UsuarioSerialiazer
from rest_framework.response import Response
from rest_framework import status

# from rest_framework.decorators import action
from django.contrib.auth.hashers import make_password

class UsuarioViewSet(viewsets.ModelViewSet):
    serializer_class = UsuarioSerialiazer
    queryset = User.objects.all()

    def create(self, request):
        '''Override, crea usuario con contraseña encriptada'''
        serializer = UsuarioSerialiazer(data = request.data)
        if (serializer.is_valid()):
            usuario = User.objects.create_user(
                username=serializer.validated_data['username'],
                password=serializer.validated_data['password'])
            return Response({"message": "Usuario creado exitosamente", "id": usuario.id}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        '''Override "necesario" para guardar la nueva contraseña encriptada, 
        actualiza los datos del usuario por pk'''
        try:
            usuario = User.objects.get(pk=pk)
        except  Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND)
        data = request.data 
        usuario.username = data.get('username', usuario.username)
        password = data.get('password')
        if password:
            usuario.password = make_password(password)
            
        usuario.save() 
        return Response(UsuarioSerialiazer(usuario).data, status=status.HTTP_200_OK)
    