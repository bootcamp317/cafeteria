from rest_framework import generics, viewsets
from .models import Producto
from .serializers import ProductoSerialiazer
from rest_framework import status
from rest_framework import authentication, permissions
from rest_framework.response import Response


class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerialiazer
    authentication_classes=[authentication.SessionAuthentication]
    permission_classes= [permissions.DjangoModelPermissions]

#     def perform_create(self, serializer):
#         #serializer.save(user=self.request.user)
#         print(serializer.validated_data)
#         title=serializer.validated_data.get('title')
#         content=serializer.validated_data.get('content') or None
        
#         if content is None:
#             content=title
#         serializer.save(content=content)

# product_list_create_view= ProductoViewSet.as_view()


# class ProductoDetailAPIView(generics.RetrieveAPIView):
#     queryset= Producto.objects.all()
#     serializer_class = ProductoSerialiazer
#     #lookup_field= 'pk

# product_detail_view= ProductoDetailAPIView.as_view()

# class ProductoPutAPIView(generics.UpdateAPIView):
#     queryset= Producto.objects.all()
#     serializer_class = ProductoSerialiazer
#     lookup_field= 'pk'
#     def perform_update(self,serializer):
#         instance= serializer.save()
#         if not instance.content:
#             instance.coontent= instance.title

# product_update_view= ProductoPutAPIView.as_view()

# class ProductoDeleteAPIView(generics.DestroyAPIView):
#     queryset= Producto.objects.all()
#     serializer_class = ProductoSerialiazer
#     lookup_field= 'pk'
#     def perform_destroy(self,instace):
#             #instace
#             super().perform_destroy(instace)

# product_delete_view= ProductoDeleteAPIView.as_view()



