from rest_framework import serializers
from .models import Producto

class ProductoSerialiazer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['id', 'nombre', 'precio']
